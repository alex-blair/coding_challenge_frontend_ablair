import request from 'superagent'

export function getUsers () {
  return dispatch => {
    return request
      .get(`https://mcc17.herokuapp.com/users`)
      .then((res) => dispatch(receiveUsers(res.body)))
  }
}

export function saveUser (userValues) {
  return dispatch => {
    return request
      .post(`https://mcc17.herokuapp.com/users`)
      .set({ 'Content-Type': 'application/json' })
      .send(userValues)
      .then((res) => dispatch(receiveUsers(res.body)))
      .catch(err => {
        return console.error(err.response.body)
      })
  }
}

export function receiveUsers (userInfo) {
  return {
    type: 'RECEIVE_USERS',
    userInfo
  }
}
