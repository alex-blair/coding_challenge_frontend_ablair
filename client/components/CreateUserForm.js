import React from 'react'
import { Field, reduxForm } from 'redux-form'

let CreateUserForm = props => {
  const { handleSubmit } = props
  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor='title'>Title</label>
        <Field
          name='title'
          component='input'
          type='text'
        />
      </div>
      <div>
        <label htmlFor='name'>Full Name</label>
        <Field
          name='name'
          component='input'
          type='text'
        />
      </div>
      <div>
        <label htmlFor='email'>Email</label>
        <Field
          name='email'
          component='input'
          type='email'
        />
      </div>
      <button type='submit'>Submit</button>
    </form>
  )
}

CreateUserForm = reduxForm({
  form: 'CreateUser'
})(CreateUserForm)

export default CreateUserForm
