import React, { Component } from 'react'
import { connect } from 'react-redux'

import { getUsers } from '../redux/actions'

import CreateUser from './CreateUser'
import ChooseExistingUser from './ChooseExistingUser'
import Header from '../components/Header'

class Home extends Component {
  componentDidMount () {
    this.props.getUsers()
  }
  render () {
    return (
      <div>
        <Header />
        <ChooseExistingUser />
        <CreateUser />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    users: state.users
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getUsers: () => {
      dispatch(getUsers())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
