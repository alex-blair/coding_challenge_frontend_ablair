import React from 'react'
import { Route } from 'react-router'

import Home from './Home'

const App = () => {
  return (
    <div>
      <Route exact path='/' component={Home} />
    </div>
  )
}

export default App
