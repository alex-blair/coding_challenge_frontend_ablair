import React, { Component } from 'react'
import { connect } from 'react-redux'

import { saveUser } from '../redux/actions'

import CreateUserForm from '../components/CreateUserForm.js'

class CreateUser extends Component {
  render () {
    const submit = (userValues) => {
      userValues.roles = ['organiser']
      console.log(userValues)
      this.props.saveUser(userValues)
    }
    return (
      <div>
        <h3>Create User</h3>
        <CreateUserForm onSubmit={submit} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userValues: state.form.createUser
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveUser: (values) => {
      dispatch(saveUser(values))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateUser)
