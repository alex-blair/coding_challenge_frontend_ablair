import test from 'tape'
import React from 'react'
import { shallow } from 'enzyme'

import Header from '../client/components/Header'

test('<Header />', t => {
  const expected = 'Morressier'
  const wrapper = shallow(<Header />)
  t.equal(wrapper.text(), expected)
  t.end()
})
